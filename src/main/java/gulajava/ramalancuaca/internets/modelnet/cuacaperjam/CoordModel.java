package gulajava.ramalancuaca.internets.modelnet.cuacaperjam;

/**
 * Created by Gulajava Ministudio.
 */
public class CoordModel {

    private double lon;
    private double lat;

    public CoordModel() {
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
