package gulajava.ramalancuaca.internets.modelnet.cuacaperjam;

/**
 * Created by Gulajava Ministudio.
 */
public class CityModel {

    private long id;
    private String name;
    private String country;
    private int population;

    private CoordModel coord;

    public CityModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public CoordModel getCoord() {
        return coord;
    }

    public void setCoord(CoordModel coord) {
        this.coord = coord;
    }
}
