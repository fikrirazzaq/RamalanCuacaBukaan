package gulajava.ramalancuaca.internets.sinkronisasi;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Gulajava Ministudio.
 * The service which allows the sync adapter framework to access the authenticator.
 */


public class RCuacaAuthenticatorService extends Service {

    // Instance field that stores the authenticator object
    private RCuacaAuthenticator mCuacaAuthenticator;

    @Override
    public void onCreate() {
        super.onCreate();

        // Create a new authenticator object
        mCuacaAuthenticator = new RCuacaAuthenticator(RCuacaAuthenticatorService.this);
    }

    /*
 * When the system binds to this Service to make the RPC call
 * return the authenticator's IBinder.
 */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mCuacaAuthenticator.getIBinder();
    }
}
