package gulajava.ramalancuaca.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.concurrent.Callable;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.database.RMCuacaHarian;
import gulajava.ramalancuaca.database.RealmKonfigurasi;
import gulajava.ramalancuaca.utilans.Konstan;
import gulajava.ramalancuaca.utilans.UtilanCuaca;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Gulajava Ministudio
 */
public class DialogDetilCuacaHarian extends DialogFragment {


    private Realm mRealm;
    private RealmQuery<RMCuacaHarian> mRealmQueryCuacaHarian;
    private RealmResults<RMCuacaHarian> mRMCuacaHarianRealmResults;
    private RealmChangeListener mRealmChangeListener;

    private boolean isDiProsesTampilan = false;

    private int barisDb;
    private RMCuacaHarian mRMCuacaHarians;
    private CancellationTokenSource mCancellationTokenSource;

    private long mStringWaktuCuaca;
    private String mStringSuhuMin;
    private String mStringSuhuMaks;
    private String mStringKodeCuaca;
    private String mStringKelembabanUdara;
    private String mStringTekananUdara;
    private String mStringArahAngin;
    private String mStringKecepatanAngin;

    private String mStringFormatHariCuaca;
    private String mStringFormatSuhuMin;
    private String mStringFormatSuhuMaks;
    private String mStringFormatKelembabanUdara;
    private String mStringFormatTekananUdara;
    private int kodeAlamatGambarCuaca;
    private String mStringNamaCuaca;
    private String mStringFormatAngin;

    @Bind(R.id.teks_detil_hari)
    TextView mTextViewWaktuCuaca;
    @Bind(R.id.teks_suhu_min)
    TextView mTextViewSuhuMin;
    @Bind(R.id.teks_suhu_max)
    TextView mTextViewSuhuMaks;
    @Bind(R.id.gambar_cuacadetil)
    ImageView mImageViewGambarCuaca;
    @Bind(R.id.teks_nama_cuaca)
    TextView mTextViewNamaCuaca;
    @Bind(R.id.teks_kelembabanudara)
    TextView mTextViewKelembabanUdara;
    @Bind(R.id.teks_tekanan_udara)
    TextView mTextViewTekananUdara;
    @Bind(R.id.teks_kecepatan_angin)
    TextView mTextViewKecepatanAngin;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        View view = LayoutInflater.from(DialogDetilCuacaHarian.this.getActivity()).inflate(R.layout.dialog_detil_harian, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(DialogDetilCuacaHarian.this.getActivity());
        builder.setView(view);
        ButterKnife.bind(DialogDetilCuacaHarian.this, view);

        builder.setNegativeButton(R.string.tomboldialog_tutup, mOnClickListenerTutup);


        Bundle bundle = DialogDetilCuacaHarian.this.getArguments();
        barisDb = bundle.getInt(Konstan.INTENT_BARISDB);


        RealmConfiguration realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(DialogDetilCuacaHarian.this.getActivity());
        mRealm = Realm.getInstance(realmConfiguration);

        mCancellationTokenSource = new CancellationTokenSource();

        //inisialisasitampilan
        inisialisasiRealm();


        Dialog dialogs = builder.create();
        dialogs.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialogs;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(DialogDetilCuacaHarian.this);

        mRealm.close();

        if (mRealmChangeListener != null && mRMCuacaHarianRealmResults != null) {
            mRMCuacaHarianRealmResults.removeChangeListeners();
        }

        mCancellationTokenSource.cancel();
    }


    DialogInterface.OnClickListener mOnClickListenerTutup = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            DialogDetilCuacaHarian.this.getDialog().dismiss();

        }
    };


    //INISIALISASI TAMPILAN
    private void inisialisasiRealm() {

        mRealmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange() {

                //setelTampilan
                if (mRMCuacaHarianRealmResults != null && mRMCuacaHarianRealmResults.isLoaded() && !isDiProsesTampilan) {

                    mRMCuacaHarians = mRMCuacaHarianRealmResults.first();

                    inisialisasiTampilan();
                }
            }
        };

        mRealmQueryCuacaHarian = mRealm.where(RMCuacaHarian.class);
        mRealmQueryCuacaHarian.equalTo("id_data", barisDb);
        mRMCuacaHarianRealmResults = mRealmQueryCuacaHarian.findAllAsync();
        mRMCuacaHarianRealmResults.addChangeListener(mRealmChangeListener);

    }


    //INISIALISASI TAMPILAN
    private void inisialisasiTampilan() {

        isDiProsesTampilan = true;

        mStringWaktuCuaca = mRMCuacaHarians.getLongwaktuMs();
        mStringSuhuMaks = mRMCuacaHarians.getTemperaturMax();
        mStringSuhuMin = mRMCuacaHarians.getTemperaturMin();
        mStringKodeCuaca = mRMCuacaHarians.getIdKodeCuaca();

        mStringKelembabanUdara = mRMCuacaHarians.getKelembabanUdara();
        mStringTekananUdara = mRMCuacaHarians.getTekananUdara();
        mStringArahAngin = mRMCuacaHarians.getArahDerajatAngin();
        mStringKecepatanAngin = mRMCuacaHarians.getKecepatanAngin();

        Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {

                mStringFormatHariCuaca = UtilanCuaca.konversiMsTanggal(mStringWaktuCuaca);

                mStringFormatSuhuMaks = UtilanCuaca.formatTemperature(DialogDetilCuacaHarian.this.getActivity(), mStringSuhuMaks);
                mStringFormatSuhuMin = UtilanCuaca.formatTemperature(DialogDetilCuacaHarian.this.getActivity(), mStringSuhuMin);

                double doubleKelembaban = Double.valueOf(mStringKelembabanUdara);
                mStringFormatKelembabanUdara = String.format(DialogDetilCuacaHarian.this.getResources().getString(R.string.format_humidity), doubleKelembaban);

                double doubleTekananUdara = Double.valueOf(mStringTekananUdara);
                mStringFormatTekananUdara = String.format(DialogDetilCuacaHarian.this.getString(R.string.format_pressure), doubleTekananUdara);

                mStringNamaCuaca = UtilanCuaca.getStatusCuacaTeks(mStringKodeCuaca);
                kodeAlamatGambarCuaca = UtilanCuaca.getGambarCuacaDariKodeKondisi(mStringKodeCuaca);

                double doubleKecepatanAngin = Double.valueOf(mStringKecepatanAngin);
                double doubleArahAngin = Double.valueOf(mStringArahAngin);
                mStringFormatAngin = UtilanCuaca.getFormattedWind(DialogDetilCuacaHarian.this.getActivity(), doubleKecepatanAngin, doubleArahAngin);

                return null;
            }
        }, mCancellationTokenSource.getToken())

                .continueWith(new Continuation<Object, Object>() {
                    @Override
                    public Object then(Task<Object> task) throws Exception {

                        isDiProsesTampilan = false;

                        mTextViewWaktuCuaca.setText(mStringFormatHariCuaca);

                        mTextViewSuhuMin.setText(mStringFormatSuhuMin);
                        mTextViewSuhuMaks.setText(mStringFormatSuhuMaks);

                        mTextViewKelembabanUdara.setText(mStringFormatKelembabanUdara);
                        mTextViewTekananUdara.setText(mStringFormatTekananUdara);

                        Glide.with(DialogDetilCuacaHarian.this).load(kodeAlamatGambarCuaca)
                                .error(R.drawable.weather_none_available)
                                .placeholder(R.drawable.weather_none_available)
                                .into(mImageViewGambarCuaca);

                        mTextViewNamaCuaca.setText(mStringNamaCuaca);

                        mTextViewKecepatanAngin.setText(mStringFormatAngin);


                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR, mCancellationTokenSource.getToken());
    }


}
