package gulajava.ramalancuaca.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.concurrent.Callable;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.database.RMCuacaPerJam;
import gulajava.ramalancuaca.database.RealmKonfigurasi;
import gulajava.ramalancuaca.utilans.Konstan;
import gulajava.ramalancuaca.utilans.UtilanCuaca;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Gulajava Ministudio
 */
public class DialogDetilCuacaPerJam extends DialogFragment {


    private Realm mRealm;
    private RealmQuery<RMCuacaPerJam> mRealmQueryCuacaJam;
    private RealmResults<RMCuacaPerJam> mRealmResultsCuacaJam;
    private RealmChangeListener mRealmChangeListener;

    private boolean isDiProsesTampilan = false;

    private int barisDb;
    private RMCuacaPerJam mRMCuacaPerJam;
    private CancellationTokenSource mCancellationTokenSource;

    private long mStringWaktuCuaca;
    private String mStringSuhuMin;
    private String mStringSuhuMaks;
    private String mStringKodeCuaca;
    private String mStringKelembabanUdara;
    private String mStringTekananUdara;
    private String mStringArahAngin;
    private String mStringKecepatanAngin;

    private String mStringFormatHariCuaca;
    private String mStringFormatJamCuaca;
    private String mStringFormatSuhuMin;
    private String mStringFormatSuhuMaks;
    private String mStringFormatKelembabanUdara;
    private String mStringFormatTekananUdara;
    private int kodeAlamatGambarCuaca;
    private String mStringNamaCuaca;
    private String mStringFormatAngin;

    @Bind(R.id.teks_detil_jam)
    TextView mTextViewWaktuCuaca;
    @Bind(R.id.teks_suhu_min)
    TextView mTextViewSuhuMin;
    @Bind(R.id.teks_suhu_max)
    TextView mTextViewSuhuMaks;
    @Bind(R.id.gambar_cuacadetil)
    ImageView mImageViewGambarCuaca;
    @Bind(R.id.teks_nama_cuaca)
    TextView mTextViewNamaCuaca;
    @Bind(R.id.teks_kelembabanudara)
    TextView mTextViewKelembabanUdara;
    @Bind(R.id.teks_tekanan_udara)
    TextView mTextViewTekananUdara;
    @Bind(R.id.teks_kecepatan_angin)
    TextView mTextViewKecepatanAngin;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        View view = LayoutInflater.from(DialogDetilCuacaPerJam.this.getActivity()).inflate(R.layout.dialog_detil_perjam, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(DialogDetilCuacaPerJam.this.getActivity());
        builder.setView(view);
        ButterKnife.bind(DialogDetilCuacaPerJam.this, view);

        builder.setNegativeButton(R.string.tomboldialog_tutup, mOnClickListenerTutup);


        Bundle bundle = DialogDetilCuacaPerJam.this.getArguments();
        barisDb = bundle.getInt(Konstan.INTENT_BARISDB);


        RealmConfiguration realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(DialogDetilCuacaPerJam.this.getActivity());
        mRealm = Realm.getInstance(realmConfiguration);

        mCancellationTokenSource = new CancellationTokenSource();

        //inisialisasitampilan
        inisialisasiRealm();


        Dialog dialogs = builder.create();
        dialogs.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialogs;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(DialogDetilCuacaPerJam.this);

        mRealm.close();

        if (mRealmChangeListener != null && mRealmResultsCuacaJam != null) {
            mRealmResultsCuacaJam.removeChangeListeners();
        }

        mCancellationTokenSource.cancel();
    }


    DialogInterface.OnClickListener mOnClickListenerTutup = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            DialogDetilCuacaPerJam.this.getDialog().dismiss();

        }
    };


    //INISIALISASI TAMPILAN
    private void inisialisasiRealm() {

        mRealmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange() {

                //setelTampilan
                if (mRealmResultsCuacaJam != null && mRealmResultsCuacaJam.isLoaded() && !isDiProsesTampilan) {

                    mRMCuacaPerJam = mRealmResultsCuacaJam.first();

                    inisialisasiTampilan();
                }
            }
        };

        mRealmQueryCuacaJam = mRealm.where(RMCuacaPerJam.class);
        mRealmQueryCuacaJam.equalTo("id_data", barisDb);
        mRealmResultsCuacaJam = mRealmQueryCuacaJam.findAllAsync();
        mRealmResultsCuacaJam.addChangeListener(mRealmChangeListener);

    }


    //INISIALISASI TAMPILAN
    private void inisialisasiTampilan() {

        isDiProsesTampilan = true;

        mStringWaktuCuaca = mRMCuacaPerJam.getWaktuUpdateMs();
        mStringSuhuMaks = mRMCuacaPerJam.getTemperaturMax();
        mStringSuhuMin = mRMCuacaPerJam.getTemperaturMin();
        mStringKodeCuaca = mRMCuacaPerJam.getIdKodeCuaca();

        mStringKelembabanUdara = mRMCuacaPerJam.getKelembabanUdara();
        mStringTekananUdara = mRMCuacaPerJam.getTekananUdara();
        mStringArahAngin = mRMCuacaPerJam.getArahDerajatAngin();
        mStringKecepatanAngin = mRMCuacaPerJam.getKecepatanAngin();

        Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {

                mStringFormatHariCuaca = UtilanCuaca.konversiMsTanggal(mStringWaktuCuaca);
                mStringFormatJamCuaca = UtilanCuaca.konversiMsJam(mStringWaktuCuaca);

                mStringFormatSuhuMaks = UtilanCuaca.formatTemperature(DialogDetilCuacaPerJam.this.getActivity(), mStringSuhuMaks);
                mStringFormatSuhuMin = UtilanCuaca.formatTemperature(DialogDetilCuacaPerJam.this.getActivity(), mStringSuhuMin);

                double doubleKelembaban = Double.valueOf(mStringKelembabanUdara);
                mStringFormatKelembabanUdara = String.format(DialogDetilCuacaPerJam.this.getResources().getString(R.string.format_humidity), doubleKelembaban);

                double doubleTekananUdara = Double.valueOf(mStringTekananUdara);
                mStringFormatTekananUdara = String.format(DialogDetilCuacaPerJam.this.getString(R.string.format_pressure), doubleTekananUdara);

                mStringNamaCuaca = UtilanCuaca.getStatusCuacaTeks(mStringKodeCuaca);
                kodeAlamatGambarCuaca = UtilanCuaca.getGambarCuacaDariKodeKondisi(mStringKodeCuaca);

                double doubleKecepatanAngin = Double.valueOf(mStringKecepatanAngin);
                double doubleArahAngin = Double.valueOf(mStringArahAngin);
                mStringFormatAngin = UtilanCuaca.getFormattedWind(DialogDetilCuacaPerJam.this.getActivity(), doubleKecepatanAngin, doubleArahAngin);

                return null;
            }
        }, mCancellationTokenSource.getToken())

                .continueWith(new Continuation<Object, Object>() {
                    @Override
                    public Object then(Task<Object> task) throws Exception {

                        isDiProsesTampilan = false;

                        String waktu = mStringFormatJamCuaca + ", " + mStringFormatHariCuaca;
                        mTextViewWaktuCuaca.setText(waktu);

                        mTextViewSuhuMin.setText(mStringFormatSuhuMin);
                        mTextViewSuhuMaks.setText(mStringFormatSuhuMaks);

                        mTextViewKelembabanUdara.setText(mStringFormatKelembabanUdara);
                        mTextViewTekananUdara.setText(mStringFormatTekananUdara);

                        Glide.with(DialogDetilCuacaPerJam.this).load(kodeAlamatGambarCuaca)
                                .error(R.drawable.weather_none_available)
                                .placeholder(R.drawable.weather_none_available)
                                .into(mImageViewGambarCuaca);

                        mTextViewNamaCuaca.setText(mStringNamaCuaca);

                        mTextViewKecepatanAngin.setText(mStringFormatAngin);


                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR, mCancellationTokenSource.getToken());
    }


}
