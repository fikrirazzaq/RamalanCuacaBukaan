package gulajava.ramalancuaca.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.joda.time.DateTime;

import butterknife.Bind;
import butterknife.ButterKnife;
import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.adapters.RecyclerCuacaHarian;
import gulajava.ramalancuaca.database.RMCuacaHarian;
import gulajava.ramalancuaca.database.RealmKonfigurasi;
import gulajava.ramalancuaca.dialogs.DialogDetilCuacaHarian;
import gulajava.ramalancuaca.utilans.Konstan;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Gulajava Ministudio
 */
public class FragmentCuacaPerHari extends Fragment {


    @Bind(R.id.recycler_list)
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;

    //data
    private RecyclerCuacaHarian mRecyclerCuacaHarian;

    //realm
    private Realm mRealm;
    private RealmQuery<RMCuacaHarian> mCuacaHarianRealmQuery;
    private RealmResults<RMCuacaHarian> mCuacaHarianRealmResults;
    private RealmChangeListener mRealmChangeListener;


    //MENERIMA PESAN BROADCAST DARI SERVIS
    private BroadcastReceiver mBroadcastReceiverServis = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String TIPE_AKSI = intent.getAction();

            if (TIPE_AKSI.contentEquals(Konstan.KODE_BROADCAST_SERVIS)) {

                //segarkan realm results
                //jika fragment sudah jalan dan sudah disegarkan
                //dari inisialisasi
                Log.w("PESAN DARI SERVIS", "FRAGMENT CUACA PER HARI MENERIMA PESAN DARI SERVIS");

            }
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = LayoutInflater.from(FragmentCuacaPerHari.this.getActivity()).inflate(R.layout.frag_cuaca_jam, container, false);
        ButterKnife.bind(FragmentCuacaPerHari.this, view);

        RealmConfiguration realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(FragmentCuacaPerHari.this.getActivity());
        mRealm = Realm.getInstance(realmConfiguration);

        inisialisasiRealmListener();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inisialisasiRealmResults();
    }

    @Override
    public void onResume() {
        super.onResume();
        FragmentCuacaPerHari.this.getActivity().registerReceiver(mBroadcastReceiverServis,
                new IntentFilter(Konstan.KODE_BROADCAST_SERVIS));
    }

    @Override
    public void onPause() {
        super.onPause();
        FragmentCuacaPerHari.this.getActivity().unregisterReceiver(mBroadcastReceiverServis);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(FragmentCuacaPerHari.this);

        mCuacaHarianRealmResults.removeChangeListeners();
        mRealm.close();
    }


    //INISIALISASI REALM LISTENER
    private void inisialisasiRealmResults() {

        //ambil waktu sekarang, dalam milidetik
        //tapi dimulai dari permulaan hari, atau 00.01 malam
        DateTime dateTimeSekarang = new DateTime();
        DateTime dateTimeMilis1Hari = new DateTime();

        long msMulaiHari = dateTimeSekarang.getMillis() - dateTimeMilis1Hari.getMillisOfDay();

        mCuacaHarianRealmQuery = mRealm.where(RMCuacaHarian.class).greaterThan("longwaktuMs", msMulaiHari);
        mCuacaHarianRealmResults = mCuacaHarianRealmQuery.findAllAsync();
        mCuacaHarianRealmResults.addChangeListener(mRealmChangeListener);

        mLinearLayoutManager = new LinearLayoutManager(FragmentCuacaPerHari.this.getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerCuacaHarian = new RecyclerCuacaHarian(FragmentCuacaPerHari.this.getActivity(), mCuacaHarianRealmResults);
        mRecyclerCuacaHarian.setOnItemClickListener(mOnItemClickListener);
        mRecyclerView.setAdapter(mRecyclerCuacaHarian);

    }


    //INISIALISASI REALM LISTENER
    private void inisialisasiRealmListener() {

        mRealmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange() {

                mRealm.refresh();

                if (mCuacaHarianRealmResults != null && mCuacaHarianRealmResults.isLoaded()) {

                    //setel dan beritahu adapter ada data yang berubah
                    mRecyclerCuacaHarian.segarkanDataRealm();
                }
            }
        };
    }


    //LISTENER RECYCLER VIEW
    RecyclerCuacaHarian.OnItemClickListener mOnItemClickListener = new RecyclerCuacaHarian.OnItemClickListener() {
        @Override
        public void onItemClick(int posisiKlik, int idDatabase) {

            DialogDetilCuacaHarian dialogDetilCuacaHarian = new DialogDetilCuacaHarian();
            Bundle bundle = new Bundle();
            bundle.putInt(Konstan.INTENT_BARISDB, idDatabase);
            dialogDetilCuacaHarian.setArguments(bundle);

            FragmentTransaction fragmentTransaction = FragmentCuacaPerHari.this.getFragmentManager().beginTransaction();
            dialogDetilCuacaHarian.show(fragmentTransaction, "DIALOG DETIL");

        }
    };


}
